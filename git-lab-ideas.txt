Ideas for Git Lab: 

0. (pretty standard) Will need everyone to set up a working git repository in everyone's computers. 
We will want everyone to at least have some experience committing something to the repository. 

For example, we can have it so that after everyone has cloned their respect repository, we can have each person
type something (undecided what something is yet) and then save a text file as <name>.txt. 

1. Then we assign them a simple math task to implement. All of them need to create a math.py that defines simple
math functions. Then they have the task of merging these things. 